import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';


declare class PlayerInfo {
  player_name: string;
  eye_point: number;
  team_point: number;
  chat_point: number;
  good_point: number;
  bad_point: number;
}

const JC: PlayerInfo = {player_name : 'Jean-Christophe Lhermitte', eye_point : 0, team_point : 0, chat_point : 0, good_point : 0, bad_point : 0}
const Loïck: PlayerInfo = {player_name : 'Loïck Thorel', eye_point : 0, team_point : 0, chat_point : 0, good_point : 0, bad_point : 0}
const Cyndie: PlayerInfo = {player_name : 'Cyndie Lefebvre', eye_point : 0, team_point : 0, chat_point : 0, good_point : 0, bad_point : 0}


@Component({
  selector: 'game-admin',
  templateUrl: './game-admin.component.html',
  styleUrls: ['./game-admin.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameAdminComponent implements OnInit {

  Players: PlayerInfo[] = [
    JC,
    Loïck,
    Cyndie
  ]

  constructor() { }

  ngOnInit(): void {

  }

  add_point(playerName, pointName): void {
    console.log(playerName + pointName)
    let player: PlayerInfo = this.Players.find(playerItem => playerItem.player_name == playerName)
    switch(pointName){
      case "eye": {
        player.eye_point++;
        break;
      }
      case "team": {
        player.team_point++;
        break;
      }
      case "chat": {
        player.chat_point++;
        break;
      }
      case "good": {
        player.good_point++;
        break;
      }
      case "bad": {
        player.bad_point++;
        break;
      }
    }
  }

  remove_point(playerName, pointName): void {
    console.log(playerName + pointName)
    let player: PlayerInfo = this.Players.find(playerItem => playerItem.player_name == playerName)
    switch(pointName){
      case "eye": {
        player.eye_point--;
        break;
      }
      case "team": {
        player.team_point--;
        break;
      }
      case "chat": {
        player.chat_point--;
        break;
      }
      case "good": {
        player.good_point--;
        break;
      }
      case "bad": {
        player.bad_point--;
        break;
      }
    }
  }
}
