import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TableListDetailsComponent } from 'app/table-list-details/table-list-details.component';
import { GameAdminComponent } from '../../game-admin/game-admin.component'
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'table-list',     component: TableListComponent },
    { path: 'table-list-detail',  component: TableListDetailsComponent},
    { path: 'game-admin',  component: GameAdminComponent},
];
