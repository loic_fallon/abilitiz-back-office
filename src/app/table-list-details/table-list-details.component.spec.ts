import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListDetailsComponent } from './table-list-details.component';

describe('TableListDetailsComponent', () => {
  let component: TableListDetailsComponent;
  let fixture: ComponentFixture<TableListDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
